//
//  DisplayFetchedImageVC.swift
//  imageGalleryApp
//
//  Created by Aditya Kalamkar on 19/11/19.
//  Copyright © 2019 Aditya Kalamkar. All rights reserved.
//

import UIKit

class DisplayFetchedImageVC: UIViewController {
    
    var someUrl = ""
    let photoImageview: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFill
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        iv.layer.masksToBounds = true
        return iv
    }()

    let backButton:UIButton = {
        let btn = UIButton()
        btn.setImage(UIImage(named: "back"), for: .normal)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.frame = CGRect(x: 30, y: 30, width: 30, height: 30)
        btn.isUserInteractionEnabled = true
        btn.addTarget(self, action: #selector(DisplayFetchedImageVC.buttonClicked(_:)), for: .touchUpInside)
        return btn
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.photoImageview.loadImageUsingCacheWithURLString(someUrl, placeHolder: UIImage(named: "placeholder"))
        print("button user: \(backButton.isUserInteractionEnabled)")
        self.photoImageview.frame = self.view.bounds
        self.view.addSubview(self.photoImageview)
        self.view.addSubview(backButton)
    }
    
    @objc func buttonClicked(_ sender: AnyObject?) {
        print("Back Pressed")
        self.dismiss(animated: true, completion: nil)
    }

}
