//
//  APIservice.swift
//  imageGalleryApp
//
//  Created by Aditya Kalamkar on 19/11/19.
//  Copyright © 2017 Aditya Kalamkar. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class APIService: NSObject {
    
    //let query = "dogs"
    lazy var endPoint: String = {
        //return "https://api.flickr.com/services/feeds/photos_public.gne?format=json&tags=\(self.query)&nojsoncallback=1#"
        return "https://jsonplaceholder.typicode.com/photos"
    }()

    func getDataWith(completion: @escaping (Result<[[String: AnyObject]]>) -> Void) {
        
        let urlString = endPoint
        
        guard let url = URL(string: urlString) else { return completion(.Error("Invalid URL, we can't update your feed")) }

        Alamofire.request(url).responseJSON(completionHandler: {response in
            print(type(of: response.result.value))
            
            guard let itemsJsonArray = response.result.value as? [[String: AnyObject]] else {
                return completion(.Error("There are no new Items to show"))
            }
            print(itemsJsonArray)
            DispatchQueue.main.async {
                completion(.Success(itemsJsonArray))
            }
        })
        /*URLSession.shared.dataTask(with: url) { (data, response, error) in
            
         guard error == nil else { return completion(.Error(error!.localizedDescription)) }
            guard let data = data else { return completion(.Error(error?.localizedDescription ?? "There are no new Items to show"))
}
            do {
                print("Data: \(data)")
                if let json = try JSONSerialization.jsonObject(with: data, options: [.mutableContainers]) as? [String: AnyObject] {
                    guard let itemsJsonArray = json["items"] as? [[String: AnyObject]] else {
                        return completion(.Error(error?.localizedDescription ?? "There are no new Items to show"))
                    }
                    DispatchQueue.main.async {
                        completion(.Success(itemsJsonArray))
                    }
                }
            } catch let error {
                return completion(.Error(error.localizedDescription))
            }
            }.resume()*/
    }
}

enum Result<T> {
    case Success(T)
    case Error(String)
}





